'''
Define the Resistor class with three data members: value, left_terminal, and
right_terminal, where value represents the resistor value in kOhms, and left_terminal
and right_terminal are nonnegative integers representing nodes in a circuit to which left and
right terminals of the resistor are connected. Define two methods for the class:
add_in_series(Resistor) and add_in_parallel(Resistor) which compute new
resistor value after adding another resistor connected in series or in parallel. The methods should
check whether creating the connection is possible i.e., whether
left_terminal/right_terminal of the first resistor is the same as
right_terminal/left_terminal of the second one, in the case of the serial connection; and
whether left_terminal and right_terminal of both resistors are connected to the same
nodes, in the case of the parallel connection. If creating a connection is impossible, the methods
should rise the InvalidConnection exception.
'''

class Resistor:
    def __init__(self, value, left_terminal, right_terminal):
        self.value = value  # kOhms
        self.left_terminal = abs(left_terminal)
        self.right_terminal = abs(right_terminal)
    def add_in_series(self, Resistor):
        if self.right_terminal == Resistor.right_terminal or self.left_terminal == Resistor.left_terminal:
            self.value += Resistor.value
        else:
             raise Exception("InvalidConnection")

    def add_in_parallel(self, Resistor):
        if self.right_terminal == Resistor.right_terminal or self.left_terminal == Resistor.left_terminal:
            self.value = (self.value * Resistor.value)/(self.value + Resistor.value)
        else:
            raise Exception("InvalidConnection")

