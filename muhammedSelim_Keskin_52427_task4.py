def degFinder(x):
    return (x[x.find('^')+1:],x[:x.find('^')-1])


def listReturner(a):
    ls = a.split()
    myL = [ls[0]]+[ls[i]+ls[i+1] for i in range(1,len(ls)-1,2)]
    myList = list(map(degFinder,myL))
    myList = sorted([(eval(it[0]),eval(it[1])) for it in myList])
    retL, subL = [], []
    for i in range(len(myList)):
        if myList[i][0] not in subL:
            subL.append(myList[i][0])
            retL.append(myList[i])
        else:
            retL[-1] = (myList[i][0],myList[i][1]+retL[-1][1])
    ### Now the true list is found, all we need to do is filling in the gaps.###
    myLen = retL[-1][0]+1
    theL = [0 for i in range(myLen)]
    for ele in retL:
        theL[ele[0]] = ele[1]
    return theL


def adder(L1,L2):
    myL = [el1 + el2 for el1,el2 in zip(L1,L2)]
    if len(L1)>len(L2):
        myL += L1[len(L2):]
    else:
        myL += L2[len(L1):]
    return myL


def substracter(L1,L2):
    L2 = [-i for i in L2]
    return adder(L1,L2)


def multiplier(L1, L2):
    m,n = len(L1),len(L2)
    retL = [0] * (m + n - 1)
    for i in range(m):
        for j in range(n):
            retL[i + j] += L1[i] * L2[j]
    return retL


def firstFinder(L):
    ind = 0
    for i in range(len(L)):
        if L[i] != 0:
            break
        ind += 1
    return ind


def printPoly(L):
    n = len(L)
    for i in range(n):
        if L[i] == 0:
            continue
        if (i != firstFinder(L) and L[i] >= 0):
            print(" + ", end="")

        if (i != firstFinder(L) and L[i] < 0):
            print(" - ", end="")

        if i == firstFinder(L):
            print(L[i], end="")
            if (i != 0):
                print("x^" + str(i), end="")

        else:
            print(abs(L[i]), end="")
            if (i != 0):
                print("x^" + str(i), end="")

def myOp(L1,L2):
    op = input("\nChoose operation: + (addition), - (subtraction), * (multiplication)\n")
    if op == "+":
        print("\nResult =",end="")
        printPoly(adder(L1,L2))
    elif op == "-":
        print("\nResult =", end="")
        printPoly(substracter(L1,L2))
    if op == "*":
        print("\nResult =", end="")
        printPoly(multiplier(L1,L2))

def Checker(a):
    for i in range(len(a)):
        if a[i] == "x" and a[i+1] != "^":
            print("\nERROR: Unrecognized character combination: {}. Try again.".format(a[i]+a[i+1]))
            return False
    return True

def Program():
    print("Welcome to the polynomial calculator. Please enter the first polynomial:")
    L1, L2 = [], []
    while True:
        s1 = input("\n")
        if(not Checker(s1)):
            continue
        L1 = listReturner(s1)
        print("First polynomial = ", end="")
        printPoly(L1)
        break

    print("\nPlease enter the second polynomial:")
    while True:
        s2 = input("\n")
        if(not Checker(s2)):
            continue
        L2 = listReturner(s2)
        print("Second polynomial = ", end="")
        printPoly(L2)
        break
    myOp(L1,L2)

    while True:
        ex = input("\nChoose: o to perform another operation with the same arguments, n to enter new arguments, x to exit\n")
        if ex == "o":
            myOp(L1, L2)
        elif ex == "n":
            print("Welcome to the polynomial calculator. Please enter the first polynomial:")
            L1, L2 = [], []
            while True:
                s1 = input("\n")
                if (not Checker(s1)):
                    continue
                L1 = listReturner(s1)
                print("First polynomial = ", end="")
                printPoly(L1)
                break

            print("\nPlease enter the second polynomial:")
            while True:
                s2 = input("\n")
                if (not Checker(s2)):
                    continue
                L2 = listReturner(s2)
                print("Second polynomial = ", end="")
                printPoly(L2)
                break
            myOp(L1, L2)

            continue

        elif ex == "x":
            print("\nBye")
            break
        else:
            continue

Program()