def shift_string(string, direction, shift_amount):
    if direction == 'left':
       
        firstString = string[:shift_amount]           
        secondString = string[shift_amount:]      
        return secondString + firstString          
    elif direction == 'right':
      
        secondString = string[-shift_amount:]        
        ind = len(string) - shift_amount           
        firstString = string[:ind]                    
        return secondString + firstString             
    else:
         return

    #call function
print(shift_string('number', 'left', 3))
print(shift_string('number', 'right', 4))
